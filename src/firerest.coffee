class DataSnapshot
	constructor: (data) ->
		@data = data

	name: () ->
		return null

	val: () ->
		return @data

class Firerest
	constructor: (url, http) ->
		if !http.get
			throw new Error "Firerest REST-object needs to have a 'get'-method"
		if !http.patch
			throw new Error "Firerest REST-object needs to have a 'patch'-method"
		if !http.put
			throw new Error "Firerest REST-object needs to have a 'put'-method"
		if !http.post
			throw new Error "Firerest REST-object needs to have a 'post'-method"
		if !http.delete
			throw new Error "Firerest REST-object needs to have a 'delete'-method"

		@http = http
		@url = url

	once: (event, successCallback) ->
		if event == 'value'
			@http.get @url + '.json', (err, data) ->
				successCallback(new DataSnapshot(data))

	set: (value, onComplete) ->
		@http.put @url + '.json', value, (err) ->
			onComplete(err)

	update: (value, onComplete) ->
		@http.patch @url + '.json', value, (err) ->
			onComplete(err)

	remove: (data, cb) ->
		#
	
if module and module.exports
	module.exports = Firerest