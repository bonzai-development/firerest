assert = require 'assert'
Firerest = require '../src/firerest'

describe 'Firerest', () ->
	describe '#constructor()', () ->
		describe 'should throw an error if REST-object has no', () ->

			it 'get-method', () ->
				assert.throws () ->
					ref = new Firerest('', {})
				, /get/

			it 'patch-method', () ->
				assert.throws () ->
					ref = new Firerest('', { 
						get: -> 
					})
				, /patch/

			it 'put-method', () ->
				assert.throws () ->
					ref = new Firerest('', { 
						get: ->
						post: -> 
					})
				, /put/

			it 'post-method', () ->
				assert.throws () ->
					ref = new Firerest('', { 
						get: -> 
					})
				, /post/

			it 'delete-method', () ->
				assert.throws () ->
					ref = new Firerest('', { 
						get: ->
						post: ->
						put: ->
					})
				, /delete/

		it 'should not throw an error if REST-object has get, post, put, and delete methods', () ->
			assert.doesNotThrow () ->
				ref = new Firerest('', { 
					get: -> 
					post: -> 
					put: ->
					delete: -> 
				})