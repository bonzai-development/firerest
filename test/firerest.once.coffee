assert = require 'assert'
Firerest = require './../src/firerest'

http = require './lib/http'

firerest = (path, http) ->
	return new Firerest('https://bonzai.firebaseio.com/firerest/test/'+path, http)

describe 'Firerest', () ->
	@timeout(60000)

	describe "#once('value', ...)", () ->
		describe "snap.val()", () ->
			it "should return null if value does not exist", (done) ->
				ref = firerest('nope', http)

				ref.once 'value', (snap) ->
					assert.equal(snap.val(), null)
					done()

			it "should return the corresponding value if it exists", (done) ->
				ref = firerest('names/1', http)

				ref.once 'value', (snap) ->
					assert.equal(snap.val(), 'henrik')
					done()

	describe '#set', () ->

		afterEach () ->
			# ref = firerest('set', http)
			# ref.remove()

		it "should set simple values", (done) ->
			ref = firerest('set/simple', http)
			data = 42

			ref.set data, (err) ->
				assert.equal(err, null)

				ref.once 'value', (snap) ->
					assert.equal(snap.val(), data)
					done()

		it "it should set object values", (done) ->
			ref = firerest('set/object', http)
			data = {
				value: 42,
				nested: {
					value: 42+1
				}
			}

			ref.set data, (err) ->
				assert.equal(err, null)

				ref.once 'value', (snap) ->
					assert.deepEqual(snap.val(), data)
					done()

	describe.only '#update', () ->

		beforeEach (done) ->
			data = {
				object: {
					existing: true,
					value: 41
				}
			}
			ref = firerest('update', http)
			ref.set data, done

		# afterEach (done) ->
		# 	ref = firerest('update', http)
			# ref.remove()

		it "should not support simple values", (done) ->
			ref = firerest('update/simple', http)
			data = 42

			ref.update data, (err) ->
				assert.notEqual(err, null)
				done()

		it "shoud update object values", (done) ->
			ref = firerest('update/object', http)
			data = {
				value: 42
			}

			ref.update data, (err) ->
				assert.equal(err, null)

				ref.once 'value', (snap) ->
					assert.deepEqual(snap.val(), {
						existing: true,
						value: 42
					})
					done()




