request = require 'request'

http = {
	get: (url, cb) ->
		request.get url, (err, res, body) ->
			cb(err, JSON.parse(body))

	post: () ->
		#
	put: (url, data, cb) ->
		request.put url, { body: JSON.stringify(data) }, (err, res, body) ->
			res = JSON.parse(body)
			cb(res?.error)
	patch: (url, data, cb) ->
		request.patch url, { body: JSON.stringify(data) }, (err, res, body) ->
			res = JSON.parse(body)
			cb(res?.error)
	delete: () ->
		#
}

module.exports = http